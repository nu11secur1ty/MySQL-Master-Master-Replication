# NOTE: MySQL must be installed and enabled
```bash
zypper in mysql
systemctl enable musql
systemctl start mysql
```

# NOTE: If you use a firewalld:
- add
```bash	
firewall-cmd --permanent --add-service=mysql
firewall-cmd --permanent --add-service=ssh
firewall-cmd --reload
```

# Create password file
```bash
vim .my.cnf
```
- add into this file

```bash	
[client]
user=_youruserr_
password=_yourpassword_
```

# Login to a MySQL and make a secure setup 
```bash
mysql_secure_installation
```
- NOTE: Use the same password from .my.cnf

- Switch to master1...
 
--------------------------------------------------------------------------------

... Coming from master1

# Check if this database is copied into master2
```bash
ls /root/mysql.sql
```

# Configure the master2 to be the slave of master1
```bash
vim /etc/my.cnf
```
- add this line if you dont have it:
```mysql
server-id   = 2
```

# Restart the mysql master2
```bash 
systemctl restart mysql
```

# Import the database from master1
```bash
mysql mysql < mysql.sql
```

# We must telling on master2 that should he must be the slave on master1
- Login to MySQl:
```mysql
change master to
-> master_host='master1',
-> master_user='youruser',
-> master_password='yourpassword',
-> master_log_file='mysql-bin.000000',     - your file bin number from master1  - THIS IS VERY IMPORTANT      
-> master_log_pos='000';                   - your Position                      - THIS IS VERY IMPORTANT
```
# Start the master2 to be the slave of master1
```mysql
start slave;
```

# Check if the slave master2 if running
```mysql
show processlist;
```
- And show the status
```mysql
show slave status \G;
```
- Otput must be
```mysql
         MariaDB [(none)]> show slave status \G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: master1
                  Master_User: repl
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000000                - your
          Read_Master_Log_Pos: 000                             - your
               Relay_Log_File: master2-relay-bin.000000        - your
                Relay_Log_Pos: 000                             - your
        Relay_Master_Log_File: mysql-bin.000000                - your
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 451
              Relay_Log_Space: 1324
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
                   Using_Gtid: No
                  Gtid_IO_Pos: 
1 row in set (0.00 sec)

ERROR: No query specified
```

-----------------------------------------------------------------------------------------------


# Telling the master2 to be the master of the master1

# Edit my.cnf config
```bash
vim /etc/my.cnf
```
-add this line if you dont have it:

```mysql
server-id   = 2
log-bin     = mysql-bin
```

# Restart the mysql
```bash
systemctl restart mysql
```

# Login to MySQL
```mysql
show master status \G;
```
- Output should be

```mysql	
 	MariaDB [(none)]> show master status \G;
*************************** 1. row ***************************
            File: mysql-bin.000000   - your file bin number  - THIS IS VERY IMPORTANT
        Position: 000                - your Position         - THIS IS VERY IMPORTANT
    Binlog_Do_DB: 
Binlog_Ignore_DB: 
1 row in set (0.00 sec)
```

- Switch to master1...

-------------------------------------------------------------------------------------------

... Coming from master1


# Check the process list
```mysql
MariaDB [(none)]> show processlist;
+----+-------------+---------------+------+-------------+------+-----------------------------------------------------------------------------+------------------+----------+
| Id | User        | Host          | db   | Command     | Time | State                                                                       | Info             | Progress |
+----+-------------+---------------+------+-------------+------+-----------------------------------------------------------------------------+------------------+----------+
|  3 | system user |               | NULL | Connect     | 4210 | Waiting for master to send event                                            | NULL             |    0.000 |
|  4 | system user |               | NULL | Connect     | 3978 | Slave has read all relay log; waiting for the slave I/O thread to update it | NULL             |    0.000 |
|  7 | repl        | master1:52382 | NULL | Binlog Dump | 4155 | Master has sent all binlog to slave; waiting for binlog to be updated       | NULL             |    0.000 |
|  9 | root        | localhost     | NULL | Query       |    0 | init                                                                        | show processlist |    0.000 |
+----+-------------+---------------+------+-------------+------+-----------------------------------------------------------------------------+------------------+----------+
4 rows in set (0.00 sec)
```

# All done ;)
