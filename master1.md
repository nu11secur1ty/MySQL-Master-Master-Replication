# Install MySQL Server
```bash
zypper in mysql
systemctl enable musql
systemctl start mysql
```

# Configure the master1 to be the master
```bash
vim /etc/my.cnf
```
- add this line if you don't have it:
```bash
server-id   = 1
log-bin     = mysql-bin
```
# Restart MySQL
```bash
systemctl restart mysql
```

# Create a password file
```bash
vim .my.cnf
```
- add into this file
```
[client]
user=_youruserr_
password=_yourpassword_
```

# NOTE: If you use a firewalld:
- add
```bash
firewall-cmd --permanent --add-service=mysql
firewall-cmd --permanent --add-service=ssh
firewall-cmd --reload
```

# Login to a MySQL and make a secure setup 
```bash  
mysql_secure_installation
```
- NOTE: Use the same password from .my.cnf 


# Create user for replication:
 - login to MySQL and add:
```mysql
grant replication slave on *.* to youruser@'%' identified by 'yourpassword';
flush privileges;
```

# Lock the tables 
```mysql
flush tables with read lock;
```
# Check the master1 status
```mysql
show master status \G;
```
- Output should be
```mysql
MariaDB [(none)]> show master status \G;
*************************** 1. row ***************************
            File: mysql-bin.000000   - your file bin number  - THIS IS VERY IMPORTANT
        Position: 000                - your Position         - THIS IS VERY IMPORTANT
    Binlog_Do_DB: 
Binlog_Ignore_DB: 
1 row in set (0.00 sec)
```
- Switch to master2...

-----------------------------------------------------------------------------------------------------

... Coming from master2

# Export the mysql database;
```bash
mysqldump mysql > mysql.sql
```

# Copy this base to master2
```bash
scp mysql.sql master1:/root/
```
- Switch to master2...

-------------------------------------------------------------------------------

... Coming from master2

# Login to MySQL
```mysql
show master status \G;
```
# And show slave status:
- NOTE: The slave status must be
```mysql
Empty set (0.00) sec)
```

# We must telling on master1 that should he must be the slave on master2
 - Login to MySQl:
```mysql
change master to
-> master_host='master2',
-> master_user='youruser',
-> master_password='yourpassword',
-> master_log_file='mysql-bin.000000',     - your file bin number from master1  - THIS IS VERY IMPORTANT      
-> master_log_pos='000';                   - your Position                      - THIS IS VERY IMPORTANT
```
# Start the slave
```mysql
start slave;
```

# And see the slave status
```mysql
show slave status \G;
```
  - Output must be
```mysql
MariaDB [(none)]> show slave status \G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: master2
                  Master_User: repl
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000000                         - your
          Read_Master_Log_Pos: 000                                      - your
               Relay_Log_File: master1-relay-bin.000000                 - your
                Relay_Log_Pos: 000                                      - your
        Relay_Master_Log_File: mysql-bin.000000                         - your
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 326
              Relay_Log_Space: 1199
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 2
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
                   Using_Gtid: No
                  Gtid_IO_Pos: 
1 row in set (0.00 sec)

ERROR: No query specified
```
# Check all setup

MariaDB [(none)]> show processlist;
```mysql
+----+-------------+---------------+------+-------------+------+-----------------------------------------------------------------------------+------------------+----------+
| Id | User        | Host          | db   | Command     | Time | State                                                                       | Info             | Progress |
+----+-------------+---------------+------+-------------+------+-----------------------------------------------------------------------------+------------------+----------+
|  3 | system user |               | NULL | Connect     | 4144 | Waiting for master to send event                                            | NULL             |    0.000 |
|  4 | system user |               | NULL | Connect     | 4084 | Slave has read all relay log; waiting for the slave I/O thread to update it | NULL             |    0.000 |
|  7 | repl        | master2:56346 | NULL | Binlog Dump | 4140 | Master has sent all binlog to slave; waiting for binlog to be updated       | NULL             |    0.000 |
|  9 | root        | localhost     | NULL | Query       |    0 | init                                                                        | show processlist |    0.000 |
+----+-------------+---------------+------+-------------+------+-----------------------------------------------------------------------------+------------------+----------+
4 rows in set (0.00 sec)
```

# All done ;)






